package net.eetg.hszg.minesweeper.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Board implements SweepListener {
	
	public int   getRows() { return fieldMatrix.length; }
	public int   getCols() { return fieldMatrix[0].length; }
	public int[] getDim()  { return new int[]{getRows(), getCols()}; }
	
	private int randomMines;
	public	int getRandomMines() { return randomMines; }
	
	private int armedFieldsCount = 0;
	public  int getArmedFieldsCount() { return armedFieldsCount; }
	
	private int sweptFieldCount = 0;
	public  int getSweptFieldCount() { return sweptFieldCount; }
	public  int unsweptRemaining() {
		return getRows() * getCols() - getArmedFieldsCount() - getSweptFieldCount();
	}
	
	private	Field[][] fieldMatrix;
	public	Field[][] getFieldMatrix() { return fieldMatrix; }
	
	/**
	 * Anzumeldende sweptField EventListener
	 */
	private List<SweepListener> sweepListenersToAdd = new ArrayList<>();
	public void addSweepListenerToAdd(SweepListener listener) {
		sweepListenersToAdd.add(listener);
	}
	
	/**
	 * Constructor
	 * @param dim Dimensionen
	 * @param mines Anzahl zufällig zu platzierender Minen
	 */
	public Board(int[] dim, int mines) {
		init(dim, mines);
	}
	
	/**
	 * Leerer Constructor -> .init(…) muss manuell aufgerufen werden.
	 * Kann verwendet werden um zusätzliche SweepListener anzumelden.
	 */
	public Board() {}
	
	/**
	 * Initialisiert das Spielbrett
	 */
	public void init(int[] dim, int mines) {
		if (dim[0] < 1) dim[0] = 1;
		if (dim[1] < 1) dim[1] = 1;
		
		int maxMineCount = dim[0] * dim[1] - 1;
		randomMines = mines < 0 ? 0 : mines > maxMineCount ? maxMineCount : mines;
		
		addSweepListenerToAdd(this);
		
		fieldMatrix = new Field[dim[0]][dim[1]];
		
		for (int i = 0; i < getRows(); i++)
			for (int j = 0; j < getCols(); j++)
				makeField(Pos.at(i, j));
		
		randomMines();
	}
	
	/**
	 * sweepingField EventListener
	 * @param field Feld
	 */
	public void sweepingField(Field field) {
		if (!field.isMine())
			sweptFieldCount++;
	}
	
	/**
	 * Initialisiert ein Feld an Position pos und meldet Listener an.
	 * @param pos Position
	 */
	private void makeField(Pos pos) {
		fieldMatrix[pos.row][pos.col] = new Field(pos);
		for (SweepListener listener : sweepListenersToAdd)
			fieldAt(pos).addSweepListener(listener);
	}
	
	/**
	 * Patrouliert um ein Feld herum:
	 * die 8 Felder um ein Feld + Feld selbst = Block aus insg. 9 Feldern;
	 * für jedes umliegende Feld (und das Ursprungsfeld) wird die Callbackfunktion ausgeführt.
	 * @param pos Position
	 * @param function Callbackfunktion
	 */
	private void patrol(Pos pos, Assault function) {
		for (int i = -1; i <= 1; i++)
			if (pos.row + i >= 0 && pos.row + i < getRows())
				for (int j = -1; j <= 1; j++)
					if (pos.col + j >= 0 && pos.col + j < getCols())
						fieldMatrix[pos.row + i][pos.col + j].callback(function);
	}
	
	/**
	 * Setzt Mine
	 * @param pos Position
	 */
	public void arm(Pos pos) {
		if (fieldAt(pos).isMine()) return;
		fieldAt(pos).setMine(true);
		armedFieldsCount++;
		patrol(pos, Field::addSurrMine);
	}
	
	/**
	 * Entschärft Mine
	 * @param pos Position
	 */
	public void disarm(Pos pos) {
		if (!fieldAt(pos).isMine()) return;
		fieldAt(pos).setMine(false);
		armedFieldsCount--;
		patrol(pos, Field::remSurrMine);
	}
	
	/**
	 * Zufällig n = randomMines Minen platzieren
	 */
	private void randomMines() {
		if (randomMines < 1)
			return;
		
		for (int i = 0; i < randomMines; i++)
			addRandomMine();
	}
	
	/**
	 * Platziert eine Mine an einer zufälligen Position
	 */
	public void addRandomMine() {
		Pos pos;
		
		do		pos = Pos.at(randNum(getRows()), randNum(getCols()));
		while	(fieldAt(pos).isMine());
		
		arm(pos);
	}
	
	/**
	 * Counts all mines on the board
	 * @return count
	 */
	public int countMines() {
		int count = 0;
		
		for (Field[] row : fieldMatrix)
			for (Field field : row)
				if (field.isMine())
					count++;
		
		return count;
	}
	
	/**
	 * Alle Felder aufdecken
	 */
	public void uncoverAll() {
		for (Field[] row: fieldMatrix)
			for (Field field: row)
				field.expose();
	}
	
	/**
	 * Jagt eine Mine in die Luft
	 * @param field Feld
	 */
	void blowUp(Field field) {
		field.sweep();
		uncoverAll();
	}
	
	/**
	 * Alle in Frage kommenden Felder ohne Minen aufdecken
	 * @param field Feld
	 */
	void sweep(Field field) {
		field.sweep();
		if (field.hasSurrMines()) return;
		
		patrol(field, next -> {
			if (next.isSwept()) return;
			sweep(next);
		});
	}
	
	/**
	 * Gibt das Feld an Position pos zurück
	 * @param pos Postion
	 * @throws ArrayIndexOutOfBoundsException Position nicht in der Matrix
	 * @return respektives Feld
	 */
	public Field fieldAt(Pos pos) throws ArrayIndexOutOfBoundsException {
		return fieldMatrix[pos.row][pos.col];
	}
	
	/**
	 * Gibt das Feld an Position pos zurück
	 * @param row Zeile
	 * @param col Spalte
	 * @throws ArrayIndexOutOfBoundsException Position nicht in der Matrix
	 * @return respektives Feld
	 */
	public Field fieldAt(int row, int col) throws ArrayIndexOutOfBoundsException {
		return fieldMatrix[row][col];
	}
	
	/**
	 * Ob Position pos innerhalb des Spielbretts liegt
	 * @param pos Position
	 * @return OutOfBounds
	 */
	boolean outOfBounds(Pos pos) {
		return pos.col < 0 || pos.row < 0 || pos.row >= getRows() || pos.col >= getCols();
	}
	
	/**
	 * Führt eine Feldaktion aus
	 * @param button Linke (0) oder rechte (1) Maustaste
	 * @param pos Position
	 */
	public void action(int button, Pos pos) {
		if (outOfBounds(pos)) return;
		
		switch (button) {
			case 0:
				clickLeft(pos);
				break;
			case 1:
				clickRight(pos);
				break;
		}
	}
	
	/**
	 * Linksklick emulieren
	 * @param pos Position
	 */
	public void clickLeft(Pos pos) {
		Field field = fieldAt(pos);
		if (field.isMine() && !field.isFlagged())
			blowUp(field);
		else
			sweep(fieldAt(pos));
	}
	
	/**
	 * Rechtsklick emulieren
	 * @param pos Position
	 */
	public void clickRight(Pos pos) {
		fieldAt(pos).toggleFlag();
	}
	
	/**
	 * Matrix im Terminal ausgeben
	 */
	public void printMatrix() {
		System.out.println();
		System.out.print(padLeft("", ("" + (fieldMatrix[0].length - 1)).length()));
		
		for (int i = 0; i < fieldMatrix[0].length; i++)
			System.out.print(padLeft("" + i, 3));
		
		System.out.println();
		
		for (int i = 0; i < fieldMatrix.length; i++)
			System.out.println(padLeft("" + i, ("" + (fieldMatrix.length - 1)).length())
					+ " " + Arrays.toString(fieldMatrix[i]));
	}
	
	/**
	 * Füllt einen String linksseitig mit Leerzeichen auf
	 * @param s String
	 * @param n Zahl links aufzufüllender Stellen
	 * @return Gepaddeter String
	 */
	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}
	
	/**
	 * Gibt eine zufällige Zahl zwischen 0 und max zurück
	 * @param max Maximum
	 * @return Zufällige Zahl
	 */
	private static int randNum(int max) {
		return new Random().nextInt(max);
	}
}
