package net.eetg.hszg.minesweeper.logic;

public class Pos {
	
	int row;
	int col;
	
	public Pos(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public Pos(int[] coords) {
		row = coords[0];
		col = coords[1];
	}
	
	public Pos(Pos pos) {
		row = pos.row;
		col = pos.col;
	}
	
	public static Pos at(int row, int col) {
		return new Pos(row, col);
	}
	
	public static Pos at(int[] coords) {
		return new Pos(coords);
	}
	
	public int[] getPosition() {
		return new int[]{row, col};
	}
	
	public boolean equals(Pos pos) {
		return pos.row == row && pos.col == col;
	}
	
	public String toString() {
		return "[" + row + "][" + col + "]";
	}
	
}
