package net.eetg.hszg.minesweeper.logic;

import net.eetg.hszg.minesweeper.console.CC;

import java.util.ArrayList;
import java.util.List;

public class Field extends Pos implements CC {
	
	private boolean mine = false;
	public boolean isMine() {
		return mine;
	}
	void setMine(boolean is) {
		mine = is;
	}
	
	private boolean flag = false;
	boolean isFlagged() {
		return flag;
	}
	void setFlag(boolean flagged) {
		flag = flagged;
	}
	void toggleFlag() {
		flag = !flag;
	}
	
	private boolean swept = false;
	public boolean isSwept() {
		return swept;
	}
	
	private boolean boom = false;
	private int surrMines = 0;
	public int getSurrMines() {
		return surrMines;
	}
	void addSurrMine() {
		surrMines++;
	}
	void remSurrMine() {
		surrMines--;
	}
	boolean hasSurrMines() {
		return surrMines > 0;
	}
	
	/**
	 * sweepListeners
	 */
	private List<SweepListener> sweepListeners = new ArrayList<>();
	public void addSweepListener(SweepListener listener) {
		sweepListeners.add(listener);
	}
	
	/**
	 * Constructor
	 * @param pos Position
	 */
	Field(Pos pos) {
		super(pos);
	}
	
	/**
	 * Feldinhalt enthüllen
	 */
	void expose() {
		swept = true;
	}
	
	/**
	 * Feld aufdecken
	 */
	void sweep() {
		if (!swept && !flag) {
			for (SweepListener listener : sweepListeners)
				listener.sweepingField(this);
			swept = true;
			boom = mine;
			for (SweepListener listener : sweepListeners)
				listener.sweptField(this);
		}
	}
	
	/**
	 * @param caller Assaultinstanz mit .function()
	 */
	void callback(Assault caller) {
		caller.function(this);
	}
	
	/**
	 * Konvertiert Field nach String
	 * @return String
	 */
	public String toColorlessString() {
		if (!swept) return flag ? "~" : "#";
		if (boom) return "B";
		if (mine) return "X";
		
		if (surrMines > 0)
			return Integer.toString(surrMines);
		
		return " ";
	}
	
	public String toString() {
		if (!swept) return flag ? CC_RED + "~" + CC_RESET : "#";
		if (boom) return CC_YELLOW_BRIGHT + "B" + CC_RESET;
		if (mine) return CC_RED_BRIGHT + "X" + CC_RESET;
		
		if (surrMines > 0)
			return MineCC[surrMines] + surrMines + CC_RESET;
		
		return " ";
	}
}
