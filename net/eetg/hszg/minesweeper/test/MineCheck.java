package net.eetg.hszg.minesweeper.test;

import net.eetg.hszg.minesweeper.logic.Pos;

public class MineCheck {
	Pos pos;
	int count;
	
	MineCheck(Pos position, int mineCount) {
		pos = position;
		count = mineCount;
	}
	
	static MineCheck at(int row, int col, int mineCount) {
		return new MineCheck(Pos.at(row, col), mineCount);
	}
}
