package net.eetg.hszg.minesweeper.console;

import net.eetg.hszg.minesweeper.logic.Board;
import net.eetg.hszg.minesweeper.logic.Field;
import net.eetg.hszg.minesweeper.logic.Pos;
import net.eetg.hszg.minesweeper.logic.SweepListener;

import java.util.Scanner;

public class MineSweeper implements SweepListener {
	
	private Board board;
	private int state = 1;
	private boolean first = true;
	
	public void sweptField(Field field) {
		if (field.isMine()) state = 2;
		
		if (board.unsweptRemaining() == 0)
			state = 3;
		
		System.out.println();
		System.out.println("sweeps remaining: " + board.unsweptRemaining());
		System.out.println("armed fields: " + board.getArmedFieldsCount());
	}
	
	public void run() {
		ask();
		console();
	}
	
	public void console() {
		state = 1;
		statePlaying();
		stateGameEnd();
		ask4Retry();
	}
	
	private void statePlaying() {
		while (state == 1) {
			board.printMatrix();
			System.out.println();
			System.out.println("button (0 or 1), row, column: ");
			String[] input = new Scanner(System.in).nextLine().split(",");
			int[] coords = new int[2];
			try {
				for (int i = 1; i < 3; i++)
					coords[i - 1] = Integer.parseInt(input[i].trim());
			} catch (Exception e) {
				System.out.println();
				System.out.println("Error. Try again.");
				continue;
			}
			Pos pos = Pos.at(coords);
			if (first) {
				first = false;
				Field firstField = board.fieldAt(pos);
				while (firstField.isMine()) {
					board.disarm(firstField);
					board.addRandomMine();
				}
			}
			board.action(Integer.parseInt(input[0]), pos);
		}
	}
	
	private void stateGameEnd() {
		board.printMatrix();
		if (state == 2) {
			System.out.println();
			System.out.println("You lost.");
		}
		if (state == 3) {
			board.uncoverAll();
			System.out.println();
			System.out.println("Congraz. You won.");
		}
	}
	
	public void ask() {
		int[] dim = ask4Dimensions();
		int mineCount = ask4MineCount();
		board = new Board();
		board.addSweepListenerToAdd(this);
		board.init(dim, mineCount);
		first = true;
	}
	
	private int[] ask4Dimensions() {
		while (true) {
			System.out.println("Enter dimensions (rows, cols): ");
			Scanner scanner = new Scanner(System.in);
			String[] input = scanner.nextLine().split(",");
			int[] dimensions = new int[2];
			try {
				for (int i = 0; i < 2; i++)
					dimensions[i] = Integer.parseInt(input[i].trim());
			} catch (Exception e) {
				System.out.println();
				System.out.println("Error. Try again.");
				continue;
			}
			return dimensions;
		}
	}
	
	private int ask4MineCount() {
		while (true) {
			System.out.println("Number'o'count: ");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.nextLine();
			int mines;
			try {
				mines = Integer.parseInt(input.trim());
				if (mines < 1) {
					System.out.println();
					System.out.println("At least one mine to be swept must exist.");
					continue;
				}
			} catch (Exception e) {
				System.out.println();
				System.out.println("Error. Try again.");
				continue;
			}
			return mines;
		}
	}
	
	private void ask4Retry() {
		boolean retry;
		
		while (true) {
			System.out.println("Play again? [Y/N]");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.nextLine().toLowerCase();
			try {
				retry = input.equals("yes") || input.equals("y");
				if (input.equals("yes") || input.equals("y") || input.equals("no") || input.equals("n"))
					break;
			} catch (Exception e) {
				//
			}
			System.out.println("Unrecognised answer.");
			System.out.println();
		}
		System.out.println();
		
		if (retry) run();
	}
	
}
