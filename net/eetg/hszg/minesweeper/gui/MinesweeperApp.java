package net.eetg.hszg.minesweeper.gui;

import net.eetg.hszg.minesweeper.logic.*;
import javafx.application.Application;
import javafx.geometry.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.stage.Stage;

public class MinesweeperApp extends Application {
	
	private Board board;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		board = new Board(new int[]{9, 9}, 10);
		
		primaryStage.setTitle("MineSweeper");
		
		MineGrid mineGrid = new MineGrid(board);
		
		Scene scene = new Scene(mineGrid);
		primaryStage.setScene(scene);
		
		scene.getStylesheets().add
				(MinesweeperApp.class.getResource("styles.css").toExternalForm());
		
		primaryStage.show();
	}
	
}
