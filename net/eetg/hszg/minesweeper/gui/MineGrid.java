package net.eetg.hszg.minesweeper.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import net.eetg.hszg.minesweeper.logic.Board;
import net.eetg.hszg.minesweeper.logic.Field;
import net.eetg.hszg.minesweeper.logic.SweepListener;

public class MineGrid extends GridPane implements SweepListener {
	
	MineGrid(Board board) {
		Field[][] fields = board.getFieldMatrix();
		this.setAlignment(Pos.CENTER);
		this.setHgap(0);
		this.setVgap(0);
		this.setPadding(new Insets(25, 25, 25, 25));
		
		for (int i = 0; i < fields.length; i++)
			for (int j = 0; j < fields[0].length; j++) {
				Field field = fields[i][j];
				MineButton btn = new MineButton(board, field);
				this.add(btn, j, i);
			}
	}
	
}
