package net.eetg.hszg.minesweeper.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import net.eetg.hszg.minesweeper.logic.Board;
import net.eetg.hszg.minesweeper.logic.Field;
import net.eetg.hszg.minesweeper.logic.SweepListener;

public class MineButton extends Button implements SweepListener {
	
	MineButton(Board board, Field field) {
		super(" ");
		field.addSweepListener(this);
		this.setOnAction(e -> board.clickLeft(field));
	}
	
	@Override
	public void sweptField(Field field) {
		this.setText(field.toColorlessString());
		this.setDisabled(true);
	}
}
